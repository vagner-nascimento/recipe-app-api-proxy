# Recipe App API Proxy

NGNIX proxy app for our recipe app API

## Usage

### Enviroment Variables

 * `LISTEN_PORT` - Port to listen on (default: `8000`)
 * `APP_HOST` - Hostname of the app to foward requests to (default: `app`)
 * `APP_PORT` - Port of the app to foward requests to (default: `9000`)

 ### Files

  * **default.conf.tpl**: The NGINX config file to work as a static file cache(hrml, css, js, etc) to a Django(Python) app. It will replaces the default by the entrypoint.sh script.

  * **entrypoint.sh**: The proxy startup script, that replaces the default config file by our custom file and prepare the enviroment to start the proxy server.